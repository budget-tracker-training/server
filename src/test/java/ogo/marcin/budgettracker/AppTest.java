package ogo.marcin.budgettracker;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@Tag("smoke")
class AppTest {

  @Test
  public void givenAppWhenStartThenApplicationStarts() {
    App.main(new String[] {});
  }
}
