package ogo.marcin.budgettracker.atdd;

import com.tngtech.jgiven.Stage;
import com.tngtech.jgiven.annotation.As;
import com.tngtech.jgiven.annotation.ProvidedScenarioState;
import com.tngtech.jgiven.integration.spring.JGivenStage;
import java.util.List;
import javax.inject.Inject;
import ogo.marcin.budgettracker.domain.Spending;
import ogo.marcin.budgettracker.domain.SpendingsSummary;
import ogo.marcin.budgettracker.domain.port.api.SpendingsPort;
import ogo.marcin.budgettracker.domain.port.api.SummaryPort;

@JGivenStage
class WhenGetSpendingsSummary extends Stage<WhenGetSpendingsSummary> {

  @ProvidedScenarioState private SpendingsSummary spendingsSummary;

  @ProvidedScenarioState private List<Spending> spendings;

  @Inject private SpendingsPort spendingsPort;

  @Inject private SummaryPort summaryPort;

  @As("I get spendings summary")
  WhenGetSpendingsSummary get_spendings_summary() {
    spendingsSummary = summaryPort.getSpendingsSummary();
    return self();
  }

  WhenGetSpendingsSummary get_all_spendings_list() {
    spendings = spendingsPort.findAll();
    return self();
  }
}
