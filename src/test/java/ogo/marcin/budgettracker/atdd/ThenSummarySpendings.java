package ogo.marcin.budgettracker.atdd;

import static org.assertj.core.api.Assertions.assertThat;

import com.tngtech.jgiven.Stage;
import com.tngtech.jgiven.annotation.ExpectedScenarioState;
import com.tngtech.jgiven.integration.spring.JGivenStage;
import java.util.List;
import ogo.marcin.budgettracker.adapter.SpendingEntity;
import ogo.marcin.budgettracker.domain.SpendingsSummary;

@JGivenStage
class ThenSummarySpendings extends Stage<ThenSummarySpendings> {

  @ExpectedScenarioState private SpendingsSummary spendingsSummary;

  @ExpectedScenarioState private List<SpendingEntity> spendings;

  ThenSummarySpendings summary_spendings_field_is_0() {
    assertThat(spendingsSummary.getSpendings()).isZero();
    return self();
  }

  ThenSummarySpendings list_of_all_spendings_with_description_is_empty() {
    assertThat(spendings).isEmpty();
    return self();
  }
}
