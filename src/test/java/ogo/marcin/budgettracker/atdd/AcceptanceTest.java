package ogo.marcin.budgettracker.atdd;

import com.tngtech.jgiven.integration.spring.EnableJGiven;
import com.tngtech.jgiven.integration.spring.junit5.SpringScenarioTest;
import com.tngtech.jgiven.junit5.JGivenExtension;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@EnableJGiven
@ExtendWith(JGivenExtension.class)
class AcceptanceTest
    extends SpringScenarioTest<
            GivenSpendingsDatabase, WhenGetSpendingsSummary, ThenSummarySpendings> {

  @Test
  @Tag("acceptance")
  void return_spendings_summary_and_spendings_list() {
    section("Summary with no spendings");

    when().get_spendings_summary().and().get_all_spendings_list();
    then().summary_spendings_field_is_0().and().list_of_all_spendings_with_description_is_empty();
  }
}
