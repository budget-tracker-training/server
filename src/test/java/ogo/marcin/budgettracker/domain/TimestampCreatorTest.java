package ogo.marcin.budgettracker.domain;

import static java.time.format.DateTimeFormatter.ISO_OFFSET_DATE_TIME;
import static java.time.format.DateTimeFormatter.ISO_ORDINAL_DATE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

import org.junit.jupiter.api.Test;

class TimestampCreatorTest {

  @Test
  void givenNoArgsConstructorThenCreateTimestampShouldNotReturnNull() {
    //    Given
    //    When
    TimestampCreator timestampCreator = new TimestampCreator();
    //    Then
    assertNotNull(timestampCreator.createTimestamp());
  }

  @Test
  void givenFixedCloakThenShouldReturnTimestampWithExpectedDateTime() {
    //    Given
    String timestampExpected = "2000-01-01T00:00:00Z";
    Clock clock = Clock.fixed(Instant.parse(timestampExpected), ZoneId.of("UTC"));
    //    When
    TimestampCreator timestampCreator =
        new TimestampCreator(ZonedDateTime.now(clock), ISO_OFFSET_DATE_TIME, ChronoUnit.SECONDS);
    //    Then
    assertEquals(timestampExpected, timestampCreator.createTimestamp());
  }

  @Test
  void givenActualTimeThenShouldNotReturnTimestampWithCorrectDateTime() {
    //    Given
    String timestampExpected = "2000-01-01T00:00:00Z";
    //    When
    TimestampCreator timestampCreator =
        new TimestampCreator(ZonedDateTime.now(), ISO_OFFSET_DATE_TIME, ChronoUnit.SECONDS);
    //    Then
    assertNotEquals(timestampExpected, timestampCreator.createTimestamp());
  }

  @Test
  void givenOrdinalFormatterThenShouldReturnTimestampWithCorrectFormat() {
    //    Given
    String timestampExpected = "2000-001Z";
    ZonedDateTime dateTime = ZonedDateTime.of(2000, 1, 1, 0, 0, 0, 1000, ZoneId.of("UTC"));
    //    When
    TimestampCreator timestampCreator =
            new TimestampCreator(dateTime, ISO_ORDINAL_DATE, ChronoUnit.SECONDS);
    //    Then
    assertEquals(timestampExpected, timestampCreator.createTimestamp());
  }
}
