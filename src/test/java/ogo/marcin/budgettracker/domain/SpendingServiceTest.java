package ogo.marcin.budgettracker.domain;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.times;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import ogo.marcin.budgettracker.adapter.SpendingEntity;
import ogo.marcin.budgettracker.adapter.SpendingEntityMapper;
import ogo.marcin.budgettracker.domain.port.spi.DbPort;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class SpendingServiceTest {

  @Mock private DbPort<SpendingEntity> dbPort;

  @Mock private SpendingEntityMapper mapper;

  @InjectMocks private SpendingsService spendingsPort;

  @Test
  void givenEmptyDbWhenFindAllThenShouldReturnEmptyList() {
    //    Given
    BDDMockito.given(dbPort.findAll()).willReturn(Collections.emptyList());

    //    When
    List<Spending> result = spendingsPort.findAll();
    //    Then
    Mockito.verify(dbPort, atLeastOnce()).findAll();
    Mockito.verify(mapper, never()).apply(Mockito.any(SpendingEntity.class));
    assertNotNull(result);
    assertThat(result).isEmpty();
  }

  @Test
  void givenSpendingWhenFindAllThenShouldReturnListWithSpending() {
    //    Given
    List<SpendingEntity> spendings = List.of(new SpendingEntity(1L, "nannanana", new BigDecimal("10.2")));
    BDDMockito.given(dbPort.findAll()).willReturn(spendings);
    BDDMockito.given(mapper.apply(BDDMockito.any(SpendingEntity.class)))
        .willReturn(new Spending("nannanana", new BigDecimal("10.2")));

    //    When
    List<Spending> result = spendingsPort.findAll();
    //    Then
    Mockito.verify(dbPort, only()).findAll();
    Mockito.verify(mapper, only()).apply(Mockito.any(SpendingEntity.class));
    assertNotNull(result);
    assertThat(result).hasSize(1);
    assertThat(result).containsExactly(new Spending("nannanana", new BigDecimal("10.2")));
  }

  @Test
  void given2SpendingsWhenFindAllThenShouldReturnListWith2Spendings() {
    //    Given
    List<SpendingEntity> spendings =
        List.of(
            new SpendingEntity(1L, "nannanana", new BigDecimal("10.2")),
            new SpendingEntity(2L, "test spending", new BigDecimal("1.22")));
    BDDMockito.given(dbPort.findAll()).willReturn(spendings);

    BDDMockito.given(mapper.apply(BDDMockito.any(SpendingEntity.class)))
        .willReturn(
            new Spending("nannanana", new BigDecimal("10.2")),
            new Spending("test spending", new BigDecimal("1.22")));

    //    When
    List<Spending> result = spendingsPort.findAll();

    //    Then
    Mockito.verify(dbPort, only()).findAll();
    Mockito.verify(mapper, times(2)).apply(Mockito.any(SpendingEntity.class));
    assertNotNull(result);
    assertThat(result).hasSize(2);
    assertThat(result)
        .containsExactly(
            new Spending("nannanana", new BigDecimal("10.2")),
            new Spending("test spending", new BigDecimal("1.22")));
  }
}
