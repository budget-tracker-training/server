package ogo.marcin.budgettracker.domain;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigDecimal;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PriceValidatorTest {

  private PriceValidator priceValidator;

  @BeforeEach
  void setup() {
    priceValidator = new PriceValidator();
  }

  @Test
  void givenCorrectPriceWhenSpendingPriceIsCorrectThenReturnTrue() {
    // Given
    BigDecimal price = new BigDecimal("10.2");
    // When
    boolean priceCorrectness = priceValidator.isSpendingPriceCorrect(price);
    // Then
    assertTrue(priceCorrectness);
  }

  @Test
  void givenNullPriceWhenSpendingPriceIsCorrectThenReturnFalse() {
    // Given
    // When
    boolean priceCorrectness = priceValidator.isSpendingPriceCorrect(null);
    // Then
    assertFalse(priceCorrectness);
  }

  @Test
  void givenZeroPriceWhenSpendingPriceIsCorrectThenReturnFalse() {
    // Given
    BigDecimal price = BigDecimal.ZERO;
    // When
    boolean priceCorrectness = priceValidator.isSpendingPriceCorrect(price);
    // Then
    assertFalse(priceCorrectness);
  }

  @Test
  void givenLowerThanZeroPriceWhenSpendingPriceIsCorrectThenReturnFalse() {
    // Given
    BigDecimal price = new BigDecimal(-11.42);
    // When
    boolean priceCorrectness = priceValidator.isSpendingPriceCorrect(price);
    // Then
    assertFalse(priceCorrectness);
  }
}
