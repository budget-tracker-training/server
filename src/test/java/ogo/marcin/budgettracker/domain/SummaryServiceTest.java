package ogo.marcin.budgettracker.domain;

import static java.time.format.DateTimeFormatter.ISO_OFFSET_DATE_TIME;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.atLeastOnce;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;
import ogo.marcin.budgettracker.domain.port.api.SpendingsPort;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class SummaryServiceTest {

  @InjectMocks private SummaryService summaryPort;

  @Mock private SpendingsPort spendingsPort;
  @Mock private TimestampCreator timestampCreator;

  @Test
  void givenEmptySpendingsWhenGetSpendingsSummaryThenShouldReturnSummaryWithZeroSpendings() {
    //    Given
    Mockito.when(spendingsPort.findAll()).thenReturn(Collections.emptyList());

    //    When
    SpendingsSummary result = summaryPort.getSpendingsSummary();

    //    Then
    Mockito.verify(spendingsPort, atLeastOnce()).findAll();
    assertNotNull(result);
    assertEquals(new BigDecimal(BigInteger.ZERO, 2), result.getSpendings());
  }

  @Test
  void givenSpendingsDataWhenGetSpendingsSummaryThenShouldReturnProperSummary() {
    //    Given
    List<Spending> spendings =
        List.of(
            new Spending("less than zero 1", new BigDecimal(1.22)),
            new Spending("less than zero 2", new BigDecimal(10.2)));
    Mockito.when(spendingsPort.findAll()).thenReturn(spendings);

    //    When
    SpendingsSummary result = summaryPort.getSpendingsSummary();

    //    Then
    Mockito.verify(spendingsPort, atLeastOnce()).findAll();
    assertNotNull(result);
    BigDecimal expected = new BigDecimal("11.42");
    assertEquals(expected, result.getSpendings());
  }

  @Test
  void givenTimestampWhenGetSpendingsSummaryThenShouldReturnSummaryCorrectTimestamp() {
    //    Given
    Mockito.when(timestampCreator.createTimestamp()).thenReturn("2000-01-01T00:00:00+01:00");

    // When
    SpendingsSummary result = summaryPort.getSpendingsSummary();

    //    Then
    Mockito.verify(timestampCreator, atLeastOnce()).createTimestamp();
    assertNotNull(result);
    assertEquals("2000-01-01T00:00:00+01:00", result.getTimestamp());
  }

  @Test
  void
      givenArgConstructorTimestampWhenGetSpendingsSummaryThenShouldReturnSummaryCorrectTimestamp() {
    //    Given
    String timestampExpected = "2000-01-01T00:00:00Z";
    Clock clock = Clock.fixed(Instant.parse(timestampExpected), ZoneId.of("UTC"));
    TimestampCreator timestampCreator =
        new TimestampCreator(ZonedDateTime.now(clock), ISO_OFFSET_DATE_TIME, ChronoUnit.SECONDS);

    Mockito.when(spendingsPort.findAll()).thenReturn(Collections.emptyList());

    //    When
    SpendingsSummary result =
        new SummaryService(spendingsPort, timestampCreator).getSpendingsSummary();

    //  Then
    Mockito.verify(spendingsPort, atLeastOnce()).findAll();

    assertNotNull(result);
    assertEquals(timestampExpected, result.getTimestamp());
  }
}
