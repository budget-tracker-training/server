package ogo.marcin.budgettracker.adapter;

import static io.restassured.http.ContentType.JSON;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.atLeastOnce;
import static org.springframework.http.HttpStatus.OK;

import io.restassured.module.mockmvc.RestAssuredMockMvc;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import ogo.marcin.budgettracker.domain.Spending;
import ogo.marcin.budgettracker.domain.SpendingsSummary;
import ogo.marcin.budgettracker.domain.port.api.SpendingsPort;
import ogo.marcin.budgettracker.domain.port.api.SummaryPort;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@Tag("integration")
class SpendingControllerAdapterTest {

  @Mock private SpendingsPort spendingsPort;
  @Mock private SummaryPort summaryPort;
  @InjectMocks private SpendingControllerAdapter spendingControllerAdapter;

  @BeforeEach
  void initialiseRestAssuredMockMvcStandalone() {
    RestAssuredMockMvc.standaloneSetup(spendingControllerAdapter);
  }

  @Test
  void givenNoExistingSpendingsWhenFindAllThenRespondWithStatusOkAndEmptyArray() {
    Mockito.when(spendingsPort.findAll()).thenReturn(Collections.emptyList());

    RestAssuredMockMvc.given()
        .when()
        .get("/spendings")
        .then()
        .statusCode(OK.value())
        .contentType(JSON)
        .body(is(equalTo("[]")));
    Mockito.verify(spendingsPort, atLeastOnce()).findAll();
  }

  @Test
  void givenSpendingWhenFindAllThenRespondWithStatusOkAndArrayWithSpending() {
    List<Spending> spendings = List.of(new Spending("nannanana", new BigDecimal("10.2")));
    Mockito.when(spendingsPort.findAll()).thenReturn(spendings);

    RestAssuredMockMvc.given()
        .when()
        .get("/spendings")
        .then()
        .statusCode(OK.value())
        .contentType(JSON)
        .body(is(equalTo("[{\"description\":\"nannanana\",\"price\":10.2}]")));
    Mockito.verify(spendingsPort, atLeastOnce()).findAll();
  }

  @Test
  void given2SpendingsWhenFindAllThenRespondWithStatusOkAndArrayWithSpending() {
    List<Spending> spendings =
        List.of(
            new Spending("nannanana", new BigDecimal("10.2")),
            new Spending("test spending", new BigDecimal("1.22")));
    Mockito.when(spendingsPort.findAll()).thenReturn(spendings);

    RestAssuredMockMvc.given()
        .when()
        .get("/spendings")
        .then()
        .statusCode(OK.value())
        .contentType(JSON)
        .body(
            is(
                equalTo(
                    "["
                        + "{\"description\":\"nannanana\",\"price\":10.2},"
                        + "{\"description\":\"test spending\",\"price\":1.22}"
                        + "]")));
    Mockito.verify(spendingsPort, atLeastOnce()).findAll();
  }

  @Test
  void givenNoSpendingsWhenGetSpendingsSummaryThenRespondWithStatusOkAndSummaryWithZeroSpendings() {
    SpendingsSummary spendingsSummary =
        new SpendingsSummary(BigDecimal.ZERO, "2000-01-01T00:00:00+01:00");
    Mockito.when(summaryPort.getSpendingsSummary()).thenReturn(spendingsSummary);

    RestAssuredMockMvc.given()
        .when()
        .get("/spendings/summary")
        .then()
        .statusCode(OK.value())
        .contentType(JSON)
        .body(is(equalTo("{\"spendings\":0,\"timestamp\":\"2000-01-01T00:00:00+01:00\"}")));
    Mockito.verify(summaryPort, atLeastOnce()).getSpendingsSummary();
  }

  @Test
  void givenSpendingsWhenGetSpendingsSummaryThenRespondWithStatusOkAndSummaryWithSpendings() {
    SpendingsSummary spendingsSummary =
        new SpendingsSummary(new BigDecimal("11.42"), "2000-01-01T00:00:00+01:00");
    Mockito.when(summaryPort.getSpendingsSummary()).thenReturn(spendingsSummary);

    RestAssuredMockMvc.given()
        .when()
        .get("/spendings/summary")
        .then()
        .statusCode(OK.value())
        .contentType(JSON)
        .body(is(equalTo("{\"spendings\":11.42,\"timestamp\":\"2000-01-01T00:00:00+01:00\"}")));
    Mockito.verify(summaryPort, atLeastOnce()).getSpendingsSummary();
  }

  @Test
  void givenSpendingsInUtcZoneWhenGetSpendingsSummaryThenRespondWithStatusOkAndProperSummary() {
    SpendingsSummary spendingsSummary =
        new SpendingsSummary(new BigDecimal("11.42"), "2000-01-01T00:00:00Z");
    Mockito.when(summaryPort.getSpendingsSummary()).thenReturn(spendingsSummary);

    RestAssuredMockMvc.given()
        .when()
        .get("/spendings/summary")
        .then()
        .statusCode(OK.value())
        .contentType(JSON)
        .body(is(equalTo("{\"spendings\":11.42,\"timestamp\":\"2000-01-01T00:00:00Z\"}")));
    Mockito.verify(summaryPort, atLeastOnce()).getSpendingsSummary();
  }
}
