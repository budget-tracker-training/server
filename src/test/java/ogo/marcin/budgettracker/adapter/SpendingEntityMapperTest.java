package ogo.marcin.budgettracker.adapter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.math.BigDecimal;
import ogo.marcin.budgettracker.domain.Spending;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class SpendingEntityMapperTest {

  private SpendingEntityMapper spendingEntityMapper;

  @BeforeEach
  void setup() {
    spendingEntityMapper = new SpendingEntityMapper();
  }

  @Test
  void givenSpendingMapShouldNotReturnNull() {
    //    Given
    SpendingEntity spendingEntity = new SpendingEntity(1L, "nannanana", new BigDecimal("10.2"));
    //    When
    //    Then
    assertNotNull(spendingEntityMapper.apply(spendingEntity));
  }

  @Test
  void givenSpendingMapShouldReturnCorrectDto() {
    //    Given
    Spending expected = new Spending("nannanana", new BigDecimal("10.2"));
    SpendingEntity spendingEntity = new SpendingEntity(1L, "nannanana", new BigDecimal("10.2"));
    //    When
    Spending spendingDto = spendingEntityMapper.apply(spendingEntity);
    //    Then
    assertEquals(expected, spendingDto);
  }
}
