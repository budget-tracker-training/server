package ogo.marcin.budgettracker.domain;

import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * Domain object of Spending.
 *
 * @author Marcin Ogorzałek
 */
@Getter
@AllArgsConstructor
@EqualsAndHashCode
@Builder
public class Spending {

  private String description;
  private BigDecimal price;
}
