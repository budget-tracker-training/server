package ogo.marcin.budgettracker.domain;

import java.math.BigDecimal;
import javax.inject.Named;

/**
 * Validates correctness of given price.
 *
 * @author Marcin Ogorzalek
 */
@Named
class PriceValidator {

  /**
   * Validate if a given price is not null and bigger then 0.
   *
   * @param price the price given to validate
   *
   * @return <code>true</code> if the given price is not null and bigger than 0, or <code>false
   *     </code> otherwise
   */
  boolean isSpendingPriceCorrect(BigDecimal price) {
    return price != null && price.compareTo(BigDecimal.ZERO) > 0;
  }
}
