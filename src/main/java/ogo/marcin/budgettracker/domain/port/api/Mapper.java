package ogo.marcin.budgettracker.domain.port.api;

import java.util.function.Function;

/**
 * Allows to map one object to another.
 *
 * @author Marcin Ogorzałek
 */
@FunctionalInterface
public interface Mapper<R, T> extends Function<R, T> {}
