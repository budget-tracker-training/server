package ogo.marcin.budgettracker.domain.port.api;

import java.util.List;
import ogo.marcin.budgettracker.domain.Spending;

/**
 * Port for obtaining list of all spendings.
 *
 * @author Marcin Ogorzałek
 */
public interface SpendingsPort {

  List<Spending> findAll();
}
