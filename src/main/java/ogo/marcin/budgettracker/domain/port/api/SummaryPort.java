package ogo.marcin.budgettracker.domain.port.api;

import ogo.marcin.budgettracker.domain.SpendingsSummary;

/**
 * Port for obtaining summary of all spendings.
 *
 * @author Marcin Ogorzalek
 */
public interface SummaryPort {

  SpendingsSummary getSpendingsSummary();
}
