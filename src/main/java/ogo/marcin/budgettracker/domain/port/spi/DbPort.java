package ogo.marcin.budgettracker.domain.port.spi;

/**
 * Port for connection with database.
 *
 * @author Marcin Ogorzałek
 */
public interface DbPort<T> {

  Iterable<T> findAll();
}
