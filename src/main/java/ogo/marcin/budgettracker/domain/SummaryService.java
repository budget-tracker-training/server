package ogo.marcin.budgettracker.domain;

import java.math.BigDecimal;
import java.math.RoundingMode;
import javax.inject.Named;
import lombok.RequiredArgsConstructor;
import ogo.marcin.budgettracker.domain.port.api.SpendingsPort;
import ogo.marcin.budgettracker.domain.port.api.SummaryPort;

/**
 * Allow to obtain sum of all persisted spendings.
 *
 * @author Marcin Ogorzalek
 */
@Named
@RequiredArgsConstructor
class SummaryService implements SummaryPort {

  private final SpendingsPort spendingsPort;
  private final TimestampCreator timestampCreator;

  /**
   * Generate summary of all users's spendings.
   *
   * @return summary of all spendings with timestamp of generation
   */
  @Override
  public SpendingsSummary getSpendingsSummary() {
    return new SpendingsSummary(sumAllSpendings(), timestampCreator.createTimestamp());
  }

  private BigDecimal sumAllSpendings() {
    return spendingsPort.findAll().stream()
        .map(Spending::getPrice)
        .reduce(BigDecimal.ZERO, BigDecimal::add)
        .setScale(2, RoundingMode.HALF_UP);
  }
}
