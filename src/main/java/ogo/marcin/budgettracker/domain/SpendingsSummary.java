package ogo.marcin.budgettracker.domain;

import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * Summary of all spendings.
 *
 * @author Marcin Ogorzalek
 */
@Getter
@AllArgsConstructor
@EqualsAndHashCode
public class SpendingsSummary {
  private final BigDecimal spendings;
  private final String timestamp;
}
