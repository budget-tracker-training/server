package ogo.marcin.budgettracker.domain;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import javax.inject.Named;
import lombok.RequiredArgsConstructor;
import ogo.marcin.budgettracker.adapter.SpendingEntity;
import ogo.marcin.budgettracker.domain.port.api.Mapper;
import ogo.marcin.budgettracker.domain.port.api.SpendingsPort;
import ogo.marcin.budgettracker.domain.port.spi.DbPort;

/**
 * Persist user's spendings in a database using SpendingRepository.
 *
 * @author Marcin Ogorzałek
 */
@Named
@RequiredArgsConstructor
class SpendingsService implements SpendingsPort {

  private final DbPort<SpendingEntity> spendingRepository;
  private final Mapper<SpendingEntity, Spending> spendingMapper;

  /**
   * Retries all user's spendings from the database and return them as list.
   *
   * @return list of all spendings.
   */
  @Override
  public List<Spending> findAll() {
    return StreamSupport.stream(spendingRepository.findAll().spliterator(), false)
        .map(spendingMapper)
        .collect(Collectors.toList());
  }
}
