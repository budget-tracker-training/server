package ogo.marcin.budgettracker.domain;

import static java.time.format.DateTimeFormatter.ISO_OFFSET_DATE_TIME;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import javax.inject.Named;
import lombok.AllArgsConstructor;

/**
 * Prepare formatted timestamp.
 *
 * <p>In the default constructor {@link DateTimeFormatter#ISO_OFFSET_DATE_TIME} and {@link
 * ChronoUnit#SECONDS} are set as datetime format and precision. Datetime used is based on {@link
 * ZonedDateTime#now()}
 *
 * <p>It it possible to change datetime, format and precision by using all argument constructor
 * instead.
 *
 * @author Marcin Ogorzalek
 */
@Named
@AllArgsConstructor
class TimestampCreator {

  private final ZonedDateTime timestamp;
  private final DateTimeFormatter dateTimeFormatter;
  private final ChronoUnit precision;

  TimestampCreator() {
    this(ZonedDateTime.now(), ISO_OFFSET_DATE_TIME, ChronoUnit.SECONDS);
  }

  /**
   * Return formatted timestamp base on given DateTimeFormatter and truncated to given precision.
   *
   * @return timestamp
   */
  String createTimestamp() {
    return timestamp.truncatedTo(precision).format(dateTimeFormatter);
  }
}
