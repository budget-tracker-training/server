package ogo.marcin.budgettracker.adapter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Represents user's spending in a database.
 *
 * @author Marcin Ogorzałek
 */
@Entity(name = "Spending")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Getter
public class SpendingEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @JsonIgnore
  private Long id;

  private String description;
  private BigDecimal price;
}
