package ogo.marcin.budgettracker.adapter;

import java.util.List;
import lombok.RequiredArgsConstructor;
import ogo.marcin.budgettracker.domain.Spending;
import ogo.marcin.budgettracker.domain.SpendingsSummary;
import ogo.marcin.budgettracker.domain.port.api.SpendingsPort;
import ogo.marcin.budgettracker.domain.port.api.SummaryPort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Contains application endpoints that allows to persis user's spendings, retries them and summary
 * as well.
 *
 * @author Marcin Ogorzałek
 */
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/spendings")
class SpendingControllerAdapter {

  private final SpendingsPort spendingsService;
  private final SummaryPort summaryPort;

  /**
   * Return all spendings stored in a database.
   *
   * @return list of all spendings
   */
  @GetMapping
  List<Spending> findAll() {
    return spendingsService.findAll();
  }

  /**
   * Return summary of spendings stored in the database along with generation timestamp.
   *
   * @return summary of all spendings
   */
  @GetMapping("summary")
  SpendingsSummary getSpendingsSummary() {
    return summaryPort.getSpendingsSummary();
  }
}
