package ogo.marcin.budgettracker.adapter;

import javax.inject.Named;
import ogo.marcin.budgettracker.domain.port.spi.DbPort;
import org.springframework.data.repository.CrudRepository;

/**
 * Contains methods to persist users's spendings in database and retrieve them back.
 *
 * @author Marcin Ogorzałek
 */
@Named
public interface SpendingRepositoryAdapter
    extends CrudRepository<SpendingEntity, Long>, DbPort<SpendingEntity> {

  Iterable<SpendingEntity> findAll();
}
