package ogo.marcin.budgettracker.adapter;

import javax.inject.Named;
import ogo.marcin.budgettracker.domain.Spending;
import ogo.marcin.budgettracker.domain.port.api.Mapper;

/**
 * {@inheritDoc} Map SpendingEntity to Spending.
 *
 * @author Marcin Ogorzałek
 */
@Named
public class SpendingEntityMapper implements Mapper<SpendingEntity, Spending> {

  /**
   * Convert SpendingEntity into Spending.
   *
   * @param spendingEntity to be converted into SpendingDto
   * @return SpendingDto
   */
  @Override
  public Spending apply(SpendingEntity spendingEntity) {
    return Spending.builder()
        .description(spendingEntity.getDescription())
        .price(spendingEntity.getPrice())
        .build();
  }
}
